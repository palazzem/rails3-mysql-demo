# Rails demo

## Used legacy stack

* Ruby 2.1.7
* Rails 3.2.22.4
* MySQL adapter
* Unicorn 4.8.3

## Backing service

* MySQL 5.6

## Getting started

Launch MySQL:

    docker-compose up -d

Set environment variables:

    export PROTECTED_USER=me
    export PROTECTED_PASSWORD=123456

Install dependencies and launch Unicorn:

    bundle install
    bundle exec unicorn -c config/unicorn.rb

## Production (you know what I mean...)

Set the following environment variables:

* ``PROTECTED_USER``
* ``PROTECTED_PASSWORD``
* ``DATABASE_URL``
* ``RAILS_ENV=production``
